import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { Typography } from "@mui/material";
import { Box } from "@mui/material";
import StyledButton from "../components/subcomponents/StyledButton";
import { styled } from "@mui/system";

const ListElement = styled("div")({
  display: "flex",
  justifyContent: "space-around",
  alignItems: "center",
  border: "solid 2px",
  padding: "10px",
  margin: "6px",
  boxSizing: "border-box",
});

export default function (props) {
  const [repoData, setRepoData] = useState({
    name: "",
    github_url: "",
    log_url: "",
    added_on: "",
    total_visits: 0,
    visits: [],
  });
  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    let token = localStorage.getItem("token");

    axios({
      method: "get",
      url: "http://localhost:3000/repo/" + params.id,
      headers: {
        Authorization: "Bearer: " + token,
      },
    }).then((response) => {
      console.log(response);
      setRepoData({
        name: response.data.repository.name,
        github_url: response.data.repository.github_url,
        log_url: response.data.repository.log_url,
        added_on: "Not implemented yet",
        total_visits: response.data.repository.visits.length,
        visits: response.data.visits,
      });
    });
  }, []);

  function handleRepoDelete() {
    let token = localStorage.getItem("token");

    axios({
      method: "post",
      url: "http://localhost:3000/repo/deleterepo",
      headers: {
        Authorization: "Bearer " + token,
      },
      data: {
        repoName: repoData.name,
      },
    }).then((response) => {
      console.log(response);
      navigate("/home");
    });
  }

  let list_components = repoData.visits.map((visit) => {
    return (
      <ListElement key={visit._id}>
        <Typography variant="body1" sx={{ fontWeight: "bolder" }}>
          {visit.request}
        </Typography>
        <Typography varaint="body1" sx={{ fontWeight: "bolder" }}></Typography>
        <Typography varaint="body1" sx={{ fontWeight: "bolder" }}>
          {visit.createdAt}
        </Typography>
        <StyledButton sx={{ width: "6%" }}>View</StyledButton>
      </ListElement>
    );
  });

  return (
    <Box>
      <Typography variant="h2">{repoData.name}</Typography>
      <Typography>Github Url: {repoData.github_url}</Typography>
      <Typography>
        Logging Url: http://localhost:3000/log/{repoData.log_url}
      </Typography>
      <Box>{list_components}</Box>
      <StyledButton onClick={handleRepoDelete}>Delete Repository</StyledButton>
    </Box>
  );
}
