import React, { useEffect, useState } from "react";

import axios from "axios";
import { Typography, Box } from "@mui/material";

export default function (props) {
  const [account, setAccount] = useState({});

  useEffect(() => {
    let token = localStorage.getItem("token");

    axios({
      method: "get",
      url: "http://localhost:3000/user/account",
      headers: {
        Authorization: "Bearer " + token,
      },
    }).then((response) => {
      setAccount(response.data.data);
    });
  }, []);

  return (
    <>
      <Typography>{account.email}</Typography>
      <Box>
        <Typography>Number of Repositories</Typography>
        <Typography>{account.repositories}</Typography>
      </Box>
    </>
  );
}
