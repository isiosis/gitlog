import React, { useContext } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Typography } from "@mui/material";
import { makeStyles } from "@mui/material";
import { styled } from "@mui/system";
import { Input, Button, Divider, Box } from "@mui/material";
import StyledButton from "../components/subcomponents/StyledButton";
import StyledInput from "../components/subcomponents/StyledInput";
import { AuthContext } from "../utils/auth";

const axios = require("axios");

const StyledDiv = styled("div")({
  border: "2px solid",
  width: "25%",
  margin: "auto",
  padding: 30,
  verticalAlign: "middle",
  height: "35%",
});

export default function (props) {
  const [inputValue, setInputValue] = useState({ email: "", password: "" });
  const context = useContext(AuthContext);

  let navigate = useNavigate();

  function handleEmailChange(e) {
    console.log(inputValue);
    setInputValue({
      email: e.target.value,
      password: inputValue.password,
    });
  }

  function handlePasswordChange(e) {
    console.log(e.target.value);
    console.log(inputValue);
    setInputValue({
      email: inputValue.email,
      password: e.target.value,
    });
  }

  function handleSubmit() {
    axios
      .post("http://localhost:3000/user/login", inputValue)
      .then((response) => {
        console.log(response);
        if (response.data.status === "success") {
          context.login(response.data.access_token);
          navigate("/home");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <Box sx={{ height: "100%", display: "flex" }}>
      <StyledDiv>
        <Box
          sx={{
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
          }}
        >
          <Typography variant="h5" sx={{ fontWeight: "bolder" }}>
            Sign into Git-Logger
          </Typography>
          <Box>
            <Typography
              variant="h6"
              color="primary"
              sx={{ fontSize: 14, fontWeight: "bold" }}
            >
              Email
            </Typography>

            <StyledInput
              type="text"
              onInput={handleEmailChange}
              placeholder="johnsmith@example.com"
            />
          </Box>
          <Box>
            <Typography
              variant="h6"
              color="primary"
              sx={{ fontSize: 14, fontWeight: "bold" }}
            >
              Password
            </Typography>

            <StyledInput
              type="password"
              onInput={handlePasswordChange}
              placeholder="********"
            />
          </Box>
          <StyledButton variant="outlined" onClick={handleSubmit}>
            Log In
          </StyledButton>
          <Divider sx={{ border: "1px solid" }} />
          <StyledButton
            variant="outlined"
            onClick={() => {
              navigate("/signup");
            }}
          >
            Sign Up
          </StyledButton>
        </Box>
      </StyledDiv>
    </Box>
  );
}
