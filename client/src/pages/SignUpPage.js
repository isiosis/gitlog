import React, { useContext } from "react";
import { useState } from "react";
import axios from "axios";
import { Typography } from "@mui/material";
import StyledButton from "../components/subcomponents/StyledButton";
import StyledInput from "../components/subcomponents/StyledInput";
import { AuthContext } from "../utils/auth";
import { useNavigate } from "react-router-dom";

export default function () {
  const [inputValue, setInputValue] = useState({
    email: "",
    password: "",
    repeatPassword: "",
  });

  const context = useContext(AuthContext);
  const navigate = useNavigate();

  function handleEmailChange(e) {
    console.log(e.target.value);
    setInputValue({
      email: e.target.value,
      password: inputValue.password,
      repeatPassword: inputValue.repeatPassword,
    });
  }

  function handlePasswordChange(e) {
    console.log(e.target.value);
    setInputValue({
      email: inputValue.email,
      password: e.target.value,
      repeatPassword: inputValue.repeatPassword,
    });
  }

  function handleRepeatPasswordChange(e) {
    console.log(e.target.value);
    setInputValue({
      email: inputValue.email,
      password: inputValue.password,
      repeatPassword: e.target.value,
    });
  }

  function handleSubmit() {
    if (inputValue.password === inputValue.repeatPassword) {
      axios
        .post("http://localhost:3000/user/signup", inputValue)
        .then((response) => {
          if (response.data.status == "success") {
            context.login(response.data.access_token);
            navigate("/home");
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  return (
    <div>
      <Typography variant="h5" sx={{ fontWeight: "bolder" }}>
        Sign up for Git-Logger
      </Typography>
      <Typography
        variant="h6"
        color="primary"
        sx={{ fontSize: 14, fontWeight: "bold" }}
      >
        Email
      </Typography>

      <StyledInput type="string" onInput={handleEmailChange} />

      <Typography
        variant="h6"
        color="primary"
        sx={{ fontSize: 14, fontWeight: "bold" }}
      >
        Password
      </Typography>
      <StyledInput type="password" onInput={handlePasswordChange} />

      <Typography
        variant="h6"
        color="primary"
        sx={{ fontSize: 14, fontWeight: "bold" }}
      >
        Repeat Password
      </Typography>
      <StyledInput type="password" onInput={handleRepeatPasswordChange} />

      <br />
      <StyledButton onClick={handleSubmit}>Sign up</StyledButton>
    </div>
  );
}
