import React, { useContext, useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import { styled } from "@mui/system";
import StyledButton from "../components/subcomponents/StyledButton";
import AddRepoComponent from "../components/AddRepoComponent";
import { AuthContext } from "../utils/auth";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const lastSeenAgo = require("last-seen-ago");

const StyledDiv = styled("div")(({ theme }) => ({
  width: "85%",
  margin: "auto",
  display: "flex",
  flexDirection: "column",
  color: theme.palette.primary.main,
}));

const ListElement = styled("div")({
  display: "flex",
  justifyContent: "space-around",
  alignItems: "center",
  border: "solid 2px",
  padding: "10px",
  margin: "6px",
  boxSizing: "border-box",
});

function calculate_last_visit(date) {
  let visit_date = new Date(date);
  return lastSeenAgo.getLastSeen(visit_date);
}

export default function (props) {
  const context = useContext(AuthContext);
  const [repos, setRepos] = useState([]);
  const [open, setOpen] = useState(false);

  let navigate = useNavigate();

  function handleModalOpen() {
    setOpen(true);
  }

  function handleModalClose() {
    setOpen(false);
  }

  function repoPageRedirect(event, id) {
    console.log(id);
    navigate("/repo/" + id);
  }

  useEffect(() => {
    let token = localStorage.getItem("token");
    axios
      .get("http://localhost:3000/repo/getrepos", {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then((response) => {
        console.log(response);
        setRepos(response.data.repositories);
      });
  }, []);

  let list_components = repos.map((repo) => {
    return (
      <ListElement key={repo._id}>
        <Typography variant="body1" sx={{ fontWeight: "bolder" }}>
          {repo.name}
        </Typography>
        <Typography varaint="body1" sx={{ fontWeight: "bolder" }}>
          View Count: {repo.visits.length}
        </Typography>
        <Typography varaint="body1" sx={{ fontWeight: "bolder" }}>
          Last Visit:{" "}
          {repo.visits.length == 0
            ? "Never"
            : calculate_last_visit(repo.lastVisit.createdAt)}
        </Typography>
        <StyledButton
          sx={{ width: "6%" }}
          onClick={(event) => repoPageRedirect(event, repo._id)}
        >
          View
        </StyledButton>
      </ListElement>
    );
  });

  return (
    <Box sx={{ height: "100%" }}>
      <StyledDiv>
        <div>
          <Typography variant="h5" sx={{ fontWeight: "bold" }}>
            Repositories
          </Typography>
        </div>
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Box
            sx={{
              width: "20%",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <section>Status</section>
            <section>Repository</section>
            <section>Num Stepper</section>
          </Box>
          <Box
            sx={{
              width: "20%",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <section>Time Range</section>
            <section>Sort</section>
          </Box>
          <AddRepoComponent open={open} handleClose={handleModalClose} />
        </Box>
        <div>{list_components}</div>
        <div>
          <StyledButton sx={{ width: "15%" }} onClick={handleModalOpen}>
            Add Repository
          </StyledButton>
        </div>
      </StyledDiv>
    </Box>
  );
}
