import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider } from "@mui/material/styles";
import logo from "./logo.svg";
import "./App.css";

import HeaderComponent from "./components/HeaderComponent";
import LoginPage from "./pages/LogInPage";
import SignUpPage from "./pages/SignUpPage";
import LandingPage from "./pages/LandingPage";
import HomePage from "./pages/HomePage";
import AuthRoute from "./components/AuthRoute";
import RepoPage from "./pages/RepoPage";
import AccountPage from "./pages/AccountPage";
import { AuthProvider } from "./utils/auth";

const custom_theme = createTheme({
  palette: {
    primary: {
      main: "#1A1A1E",
      light: "#777777",
    },
  },

  typography: {
    fontFamily: "Inter",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    fontSize: 16,
  },
});

function App() {
  return (
    <>
      <AuthProvider>
        <ThemeProvider theme={custom_theme}>
          <Router>
            <HeaderComponent />
            <Routes>
              <Route exact path="/" element={<LandingPage />} />
              <Route exact path="/signup" element={<SignUpPage />} />
              <Route exact path="/login" element={<LoginPage />} />
              <Route
                exact
                path="/home"
                element={<AuthRoute Component={HomePage} />}
              />
              <Route exact path="/repo/:id" element={<RepoPage />} />
              <Route exact path="/account" element={<AccountPage />} />
            </Routes>
          </Router>
        </ThemeProvider>
      </AuthProvider>
    </>
  );
}

export default App;
