import React, { useReducer, createContext } from "react";
import jwt_decode from "jwt-decode";

const initialState = {
  user_email: null,
};

if (localStorage.getItem("token")) {
  let user_email = jwt_decode(localStorage.getItem("token")).email;

  initialState.user_email = user_email;
}

const AuthContext = createContext({
  user_email: null,
  login: (data) => {},
  logout: () => {},
});

function authReducer(state, action) {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        user_email: action.payload,
      };
    case "LOGOUT":
      return {
        ...state,
        user_email: null,
      };

    default:
      return state;
  }
}

function AuthProvider(props) {
  const [state, dispatch] = useReducer(authReducer, initialState);

  function login(token) {
    console.log("token: ", token);
    let user_email = jwt_decode(token).email;
    localStorage.setItem("token", token);
    dispatch({
      type: "LOGIN",
      payload: user_email,
    });
  }

  function logout() {
    localStorage.removeItem("token");
    dispatch({
      type: "LOGOUT",
    });
  }

  return (
    <AuthContext.Provider
      value={{ user_email: state.user_email, login, logout }}
      {...props}
    />
  );
}

export { AuthProvider, AuthContext };
