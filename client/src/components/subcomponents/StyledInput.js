import React from "react";
import { styled } from "@mui/system";

export default styled("input")(({ theme }) => ({
  border: "2px solid",
  width: "100%",
  borderRadius: 0,
  boxSizing: "border-box",
  height: 30,
  padding: 5,
  fontWeight: "bold",
  fontFamily: "Inter",
  color: theme.palette.primary.light,
  borderColor: theme.palette.primary.main,
  "&:focus": {
    borderRadius: 0,
    outline: "none",
    color: theme.palette.primary.main,
  },
}));
