import React from "react";
import { Button } from "@mui/material";
import { styled } from "@mui/system";

export default styled(Button)({
  borderRadius: 0,
  border: "2px solid",
  boxShadow: "2px 2px #666666",
  fontWeight: "bold",
  width: "100%",
  height: 30,
  "&:hover": {
    boxShadow: "none",
  },
  textTransform: "none",
  fontSize: "12px",
});
