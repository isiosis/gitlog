import { Modal, Typography } from "@mui/material";
import React from "react";
import StyledInput from "./subcomponents/StyledInput";
import StyledButton from "./subcomponents/StyledButton";
import { useState } from "react";
import { Box } from "@mui/material";
import axios from "axios";

export default function (props) {
  const [inputValue, setInputValue] = useState({ repoName: "", repoUrl: "" });

  function handleRepoAdd() {
    let token = localStorage.getItem("token");

    axios({
      method: "post",
      url: "http://localhost:3000/repo/addrepo",
      headers: {
        Authorization: "Bearer " + token,
      },
      data: inputValue,
    }).then((response) => {
      console.log(response);
    });
  }

  function handleRepoNameChange(e) {
    setInputValue({
      ...inputValue,
      repoName: e.target.value,
    });
  }

  function handleRepoUrlChange(e) {
    setInputValue({
      ...inputValue,
      repoUrl: e.target.value,
    });
  }

  return (
    <>
      <Modal open={props.open} onClose={props.handleClose}>
        <Box>
          <Box>
            <Typography variant="h6">Repository Name</Typography>
            <StyledInput
              placeholder={
                inputValue.repoName
                  ? inputValue.repoName
                  : "Enter repository name"
              }
              onInput={handleRepoNameChange}
            />
          </Box>
          <Box>
            <Typography variant="h6">Github Url</Typography>
            <StyledInput
              placeholder={
                inputValue.repoUrl ? inputValue.repoUrl : "Enter repository url"
              }
              onInput={handleRepoUrlChange}
            />
          </Box>
          <Box>
            <StyledButton onClick={props.handleClose}>Close Modal</StyledButton>
            <StyledButton onClick={handleRepoAdd}>Add Repository</StyledButton>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
