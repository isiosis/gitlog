import { MenuItem, Select } from "@mui/material";
import React, { useContext } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { AuthContext } from "../utils/auth";

export default function () {
  const context = useContext(AuthContext);

  let navigate = useNavigate();

  function handleLogout() {
    context.logout();
  }

  function handleAccountPage() {
    navigate("/account");
  }

  return (
    <div>
      <Link to="/">Home</Link>

      {context.user_email ? (
        <>
          <Select label="icon">
            <MenuItem onClick={handleAccountPage}>Account</MenuItem>
            <MenuItem onClick={handleLogout}>Log out</MenuItem>
          </Select>
        </>
      ) : (
        <>
          <Link to="/login">Login</Link>
          <Link to="/signup">Sign Up</Link>
        </>
      )}
    </div>
  );
}
