import React, { useContext } from "react";
import { Route, Navigate } from "react-router-dom";

import { AuthContext } from "../utils/auth";

function AuthRoute({ Component }) {
  const { user_email } = useContext(AuthContext);

  return user_email ? <Component /> : <Navigate to="/login" />;
}

export default AuthRoute;
