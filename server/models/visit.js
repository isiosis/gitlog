const mongoose = require("mongoose");

const VisitSchema = new mongoose.Schema(
  {
    request: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Visit", VisitSchema);
