const mongoose = require("mongoose");
const Repository = require("./repository");
const Visit = require("./visit");
require("mongoose-type-email");
var bcrypt = require("bcrypt");
var SALT_WORK_FACTOR = 10;

function generateRandomString() {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
  var charactersLength = characters.length;
  for (var i = 0; i < 7; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

async function makeLogUrl() {
  let log_url = generateRandomString();
  let return_val = await Repository.findOne({ log_url: log_url });

  while (return_val != null) {
    log_url = generateRandomString();
    return_val = await Repository.findOne({ log_url: log_url });
  }

  return log_url;
}

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  repositories: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Repository",
    },
  ],
});

UserSchema.pre("save", function (next) {
  var user = this;

  if (!user.isModified("password")) return next();

  bcrypt.hash(user.password, SALT_WORK_FACTOR, function (err, hash) {
    if (err) return next(err);
    user.password = hash;
    next();
  });
});

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    if (isMatch) return cb(err, isMatch);
    //return no error if isMatch (true)
    else return cb(true); //returning a bogus value of true to show that there is an error
    //and that passwords are not the same
  });
};

UserSchema.methods.addRepo = function (repoName, repoUrl) {
  return new Promise(async (resolve, reject) => {
    let log_url = await makeLogUrl();

    let duplicate_repo = await Repository.findOne({ name: repoName });

    if (duplicate_repo != null) {
      reject(new Error("Repository name already in use"));
      return;
    }

    var repository = new Repository({
      name: repoName,
      github_url: repoUrl,
      log_url: log_url,
      visits: [],
    });

    repository
      .save()
      .then((res) => {
        this.repositories.push(repository);
        return this.save();
      })
      .then(() => {
        resolve();
      })
      .catch((err) => {
        reject(err);
        return;
      });
  });
};

UserSchema.methods.deleteRepo = function (repoName) {
  //TODO: Check if the repository belongs to this user
  return new Promise((resolve, reject) => {
    Repository.findOne({ name: repoName })
      .then((repository) => {
        if (repository == null) {
          reject(new Error("No repository by that name exists"));
          return;
        }
        for (let i = 0; i < this.repositories.length; i++) {
          if (this.repositories[i].toString() == repository.toString()) {
            this.repositories.splice(i, 1);
          }
        }

        return repository.delete();
      })
      .then(() => {
        console.log(this.repositories);
        resolve();
      })
      .catch((err) => {
        reject(err);
        return;
      });
  });
};

UserSchema.methods.getRepos = function () {
  return new Promise(async (resolve, reject) => {
    if (this.repositories.length == 0) {
      resolve([]);
    }
    let repositories = this.repositories.map(async (repository) => {
      let repository_data = Repository.findById(repository);
      return repository_data;
    });

    repositories = await Promise.all(repositories);

    console.log(repositories);

    repositories_list = [];
    for (let i = 0; i < repositories.length; i++) {
      let temp = repositories[i].toObject();

      if (temp.visits.length != 0) {
        temp["lastVisit"] = await Visit.findById(
          temp.visits[temp.visits.length - 1]
        );
      } else {
        temp["lastVisit"] = "None";
      }
      repositories_list.push(temp);
    }

    resolve(repositories_list);
  });
};

UserSchema.methods.getRepo = function (id) {
  return new Promise(async (resolve, reject) => {
    let found = false;
    this.repositories.every((repo) => {
      if (id == repo) {
        found = true;
        return false;
      } else {
        return true;
      }
    });

    console.log(found);

    if (!found) reject(new Error("Not owner of repository"));

    let repo = await Repository.findById(id);
    let visits = [];

    for (let i = 0; i < repo.visits.length; i++) {
      visits.push(await Visit.findById(repo.visits[i]));
    }

    resolve({
      repo: repo,
      visits: visits,
    });
  });
};
module.exports = mongoose.model("User", UserSchema);
