const mongoose = require("mongoose");
var Visit = require("./visit");

const RepositorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  github_url: {
    type: String,
    required: true,
  },
  log_url: {
    type: String,
    required: true,
    unique: true,
  },
  visits: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Visit",
    },
  ],
});

RepositorySchema.methods.logVisit = function (request) {
  var requestString = JSON.stringify(request);
  var newVisit = new Visit({ request: requestString });
  newVisit.save();
  this.visits.push(newVisit);
};

module.exports = mongoose.model("Repository", RepositorySchema);
