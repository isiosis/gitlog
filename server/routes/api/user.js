var express = require("express");
var bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");

var router = express.Router();
router.use(bodyParser.json());

var User = require("../../models/user");

function generateAccessToken(email) {
  return jwt.sign({ email: email }, process.env.TOKEN_SECRET, {
    expiresIn: "1800s",
  });
}

function decodeAccessToken(token) {
  let response = jwt.verify(token.split(" ")[1], process.env.TOKEN_SECRET);
  return response.email;
}

router
  .post("/signup", (req, res, next) => {
    try {
      User.find({ email: req.body.email })
        .then((person) => {
          if (person.length > 0) {
            throw new Error("Email already in use");
          }
          //create a new user object from schema
          var user = new User({
            email: req.body.email,
            password: req.body.password,
          });
          //save the created person
          return user.save();
        })
        .then(() => {
          const token = generateAccessToken(req.body.email);
          let response = {
            status: "success",
            message: "successfully signed up",
            access_token: token,
          };
          res.send(response);
        })
        .catch((err) => {
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  })
  .post("/login", (req, res, next) => {
    try {
      User.findOne({ email: req.body.email })
        .then((user) => {
          user.comparePassword(req.body.password, function (err, result) {
            //if not the same password hash throw error
            if (err) {
              return next(new Error("Wrong email or password"));
            }

            const token = generateAccessToken(req.body.email);

            let response = {
              status: "success",
              message: "successfully logged in",
              access_token: token,
            };

            res.send(response);
          });
        })
        .catch((err) => {
          return next(new Error("Wrong email or password"));
        });
    } catch (err) {
      next(err);
      return;
    }
  })
  .get("/account/", (req, res, next) => {
    try {
      if (!("authorization" in req.headers))
        throw new Error("Missing authorization header");

      let email = decodeAccessToken(req.headers["authorization"]);

      User.findOne({ email: email })
        .then((user) => {
          let response = {
            status: "Success",
            message: "Successfully fetched all information",
            data: {
              email: user.email,
              repositories: user.repositories.length,
            },
          };

          res.send(response);
        })
        .catch((err) => {
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  });

module.exports = router;
