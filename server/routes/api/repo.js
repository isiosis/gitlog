var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");
var User = require("../../models/user");
var Repository = require("../../models/repository");

function decodeAccessToken(token) {
  let response = jwt.verify(token.split(" ")[1], process.env.TOKEN_SECRET);
  return response.email;
}

/* GET home page. */
router
  .post("/addrepo", function (req, res, next) {
    try {
      if (!("authorization" in req.headers))
        throw new Error("Missing authorization header");

      let email = decodeAccessToken(req.headers["authorization"]);
      let repoName = req.body.repoName;
      let repoUrl = req.body.repoUrl;

      User.findOne({ email: email })
        .then((user) => {
          return user.addRepo(repoName, repoUrl);
        })
        .then(() => {
          let response = {
            status: "Success",
            message: "Successfully added repository",
          };

          res.send(response);
        })
        .catch((err) => {
          console.log(err.message);
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  })
  .post("/deleterepo", function (req, res, next) {
    try {
      if (!("authorization" in req.headers))
        throw new Error("Missing authorization header");

      let email = decodeAccessToken(req.headers["authorization"]);
      let repoName = req.body.repoName;

      User.findOne({ email: email })
        .then((user) => {
          return user.deleteRepo(repoName);
        })
        .then(() => {
          return user.save();
        })
        .then(() => {
          let response = {
            status: "Success",
            message: "Successfully deleted repository",
          };

          res.send(response);
        })
        .catch((err) => {
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  })
  .get("/getrepos", function (req, res, next) {
    try {
      if (!("authorization" in req.headers))
        throw new Error("Missing authorization header");
      let email = decodeAccessToken(req.headers["authorization"]);

      User.findOne({ email: email })
        .then((user) => {
          return user.getRepos();
        })
        .then((repositories) => {
          console.log(repositories);
          let response = {
            status: "Success",
            message: "Successfully retrieved repositories",
            repositories: repositories,
          };

          res.send(response);
        })
        .catch((err) => {
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  })
  .get("/:id", function (req, res, next) {
    try {
      if (!("authorization" in req.headers))
        throw new Error("Missing authorization header");
      let email = decodeAccessToken(req.headers["authorization"]);

      User.findOne({ email: email })
        .then((user) => {
          return user.getRepo(req.params.id);
        })
        .then(({ repo, visits }) => {
          let response = {
            status: "Success",
            message: "Successfully retrieved repository information",
            repository: repo,
            visits: visits,
          };

          res.send(response);
        })
        .catch((err) => {
          return next(err);
        });
    } catch (err) {
      return next(err);
    }
  });

module.exports = router;
