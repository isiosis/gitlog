var express = require("express");
var router = express.Router();
var Repository = require("../../models/repository");

router.get("/:logUrl", function (req, res, next) {
  try {
    Repository.findOne({ log_url: req.params.logUrl })
      .then((repository) => {
        repository.logVisit(req.headers);
        repository.save();
      })
      .catch((err) => {
        next(err);
      });
    res.send("");
  } catch (err) {
    next(err);
  }
});

module.exports = router;
